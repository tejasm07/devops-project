##installation of helm on linux:: 

curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
----------------------------------------------------------------------------------------------------------
step to start creating helm chart

1) helm create ui-deploy
  
** take deployment file from k8s_dev.yaml :: and use in template deployment.yaml
**take service file from  dev.yaml :: and use in template service.yaml
**take hpa file from prod.yaml :: and used in template hpa.yaml

a) in _helpers.tpl: replace all our matchlabel metadata

b) check the validation part using 
helm template dev-templ -f ./ui-deploy/values-dev.yaml ./ui-deploy --debug

c) make hpa enable , false in values-dev,values-qa

d) req and limit that we mention is remove from deployment.yaml and replace with computers block with toyaml to call complete value

helm template dev-templ -f ./ui-deploy/values-qa.yaml ./ui-deploy > qa.yaml 
##redirect file to qa.yaml